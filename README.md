# Micro Tetris

Un gioco simile a tetris fatto di blocchi dello stesso tipo.

Ogni blocco e' rappresentato da una luce rossa sul display del microbit.
Partendo dalla prima posizione in alto a sinistra, la luce si spostera piano piano verso il fondo.
Si possono usare i due pulsanti per muovere la luce a destra e a sinistra.
Bisogna posizionare i led in fila per formare una riga e quindi elliminare la riga cosi formata.
Se una colona viene riempita per 5 posizioni e' game over.